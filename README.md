Introdução

Olá !

Seja bem-vindo ao **Projeto 2 - Proxy IRC com Web Services e Web Sockets do Grupo 2** da matéria Aplicações Distribuídas, do Grupo 2!

Disciplina ministrada pelo **Professor Me. Marcelo Akira**!

Os membros participantes deste Grupo são:


- @fjcs7  - Líder/Desenvolvedor :crown: :computer:

- @bbrazsilveira - Desenvolvedor :computer: 

- @matheusgalhardo - Documentador :writing_hand: 

- @alexmaia.ufg   - Documentador :writing_hand:


### Como utilizar

Antes de rodar o servidor, precisamos entrar na pasta dele e instalar as dependências do NodeJS `npm install`.

```shell
$ cd back-end
$ npm install
```

Agora vamos entrar na pasta `src` e rodar o `app.js`

```shell
$ cd src
$ node app.js
```

Pronto! Agora você deve ser capaz de acessar o servidor em `http://localhost:3000`


### Quer saber mais sobre nosso projeto?

Acesse a documentação completa na [Wiki](https://gitlab.com/ad-si-2017-2/p2-g2/wikis/home) do nosso projeto.

Projeto 2 - protótipo de cliente web para servidor IRC da disciplina de aplicações/sistemas distribuídos

