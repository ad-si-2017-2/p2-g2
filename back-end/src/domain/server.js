var Client = require('./client');

function Server(io) {
    this.proxy = {};
    this.io = io;
}

Server.prototype.init = function() {
    var _this = this;
    this.io.on("connection", function(socket) {
        // Add new user
        var client = new Client(socket.id, socket);
        _this.addClientOnProxy(client);
        console.log("Conectado " + socket.id);
        console.log("Total de clientes: " + Object.keys(_this.proxy).length);

        // Register Event
        socket.on("register", function(nickname, server) {
            client.register(nickname, server);
        });

        // List Event
        socket.on("list", function() {
            client.list();
        });

        // Join Event
        socket.on("join", function(channel) {
            client.join(channel);
        });

        // Say Event
        socket.on("say", function(channel, message) {
            client.say(channel, message);
        });

        // Part Event
        socket.on("part", function(channel, message) {
            client.part(channel, message);
        });

        // Send Event
        socket.on("send", function(command, args) {
            console.log("send: " + command + " - " + args);
            client.send(command, args);
        });

        // Disconnect Event
        socket.on("disconnect", function() {
            console.log("Desconectado " + socket.id);
            _this.removeClientOnProxy(socket.id);
        });
    });
}

Server.prototype.addClientOnProxy = function(client) {
    this.proxy[client.id] = client;
};


Server.prototype.removeClientOnProxy = function(client_id) {
    var irc = this.proxy[client_id].irc;
    if (irc != null) {
        irc.disconnect();
    }
    delete this.proxy[client_id];
};


module.exports = Server;