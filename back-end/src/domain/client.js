var irc = require('irc');

function Client(id, socket) {
    this.id = id;
    this.socket = socket;
    this.irc = null;
}

/* Methods */

Client.prototype.register = function(nickname, server) {
    _this = this;
    this.irc = new irc.Client(server, nickname, { showErrors: true });
    this.irc.addListener('error', (message) => this.onError(message));
    this.irc.addListener('registered', (message) => this.onRegister(message));
    this.irc.addListener('channellist', (channel_list) => this.onChannelList(channel_list));
    this.irc.addListener('join', (channel, nick, message) => this.onJoin(channel, nick, message));
    this.irc.addListener('names', (channel, nicks) => this.onNames(channel, nicks));
    this.irc.addListener('part', (channel, nick, reason, message) => this.onPart(channel, nick, reason, message));
    this.irc.addListener('quit', (nick, reason, channels, message) => this.onQuit(nick, reason, channels, message));
    this.irc.addListener('kick', (channel, nick, by, reason, message) => this.onKick(channel, nick, by, reason, message));
    this.irc.addListener('kill', (nick, reason, channels, message) => this.onKill(nick, reason, channels, message));
    this.irc.addListener('message', (nick, to, text, message) => this.onMessage(nick, to, text, message));
    this.irc.addListener('whois', (info) => this.onWhois(info));
    this.irc.addListener('raw', (message) => this.onRaw(message));
    this.irc.addListener('nick', (oldnick, newnick, channels, message) => this.onNick(oldnick, newnick, channels, message));

    console.log("Registrando " + this.id);
};

Client.prototype.list = function() {
    this.irc.list();
};

Client.prototype.join = function(channel) {
    this.irc.join(channel);
};

Client.prototype.say = function(channel, message) {
    if (message[0] == '/') {
        this.onCommandReceived(message);
    } else {
        this.irc.say(channel, message);
    }
};

Client.prototype.part = function(channel, message) {
    if (message != null && message.length > 0) {
        this.irc.part(channel, message);
    } else {
        this.irc.part(channel, "");
    }
};

Client.prototype.send = function(command, args) {
    this.irc.send(command, args);
};

/* Events */

Client.prototype.onError = function(message) {
    console.error('ERROR: %s: %s', message.command, message.args.join(' '));
}

Client.prototype.onRegister = function(message) {
    this.socket.emit('register', message.args[0], message.args[1]);
}

Client.prototype.onChannelList = function(channel_list) {
    this.socket.emit('channellist', channel_list);
}

Client.prototype.onJoin = function(channel, nick, message) {
    this.socket.emit('join', channel, nick, message);
}

Client.prototype.onNames = function(channel, nicks) {
    this.socket.emit('names', channel, nicks);
}

Client.prototype.onPart = function(channel, nick, reason, message) {
    this.socket.emit('part', channel, nick, reason, message);
}

Client.prototype.onQuit = function(nick, reason, channels, message) {
    this.socket.emit('quit', nick, reason, channels, message);
}

Client.prototype.onKick = function(channel, nick, by, reason, message) {
    this.socket.emit('kick', channel, nick, by, reason, message);
}

Client.prototype.onKill = function(nick, reason, channels, message) {
    this.socket.emit('kill', nick, reason, channels, message);
}

Client.prototype.onMessage = function(nick, to, text, message) {
    this.socket.emit('message', nick, to, text, message);
}

Client.prototype.onWhois = function(info) {
    this.socket.emit('whois', info);
}

Client.prototype.onRaw = function(message) {
    this.socket.emit('raw', message);
}

Client.prototype.onNick = function(oldnick, newnick, channels, message) {
    this.socket.emit('nick', oldnick, newnick, channels, message);
}

Client.prototype.onCommandReceived = function(commandData) {
    var params;

    // Split command data and check if contains any text defined by ' :'
    if (commandData.indexOf(" :") !== -1) {
        var paramsWithText = commandData.split(" :");
        params = paramsWithText[0].split(" ");
        params.push(paramsWithText[1]);
    } else {
        params = commandData.split(" ");
    }

    // Get command from parameters
    var command = params[0].toUpperCase();
    command = command.replace("/", "");

    try {
        console.log("onCommandReceived: ", command, JSON.stringify(commandData));
        // Check command sent and delegate it
        if (command === "PASS") {
            console.log("PASS: " + command);
            //connection.pass(server, user, params[1]);
        } else if (command === "NICK") {
            console.log("NICK", params[1])
            this.irc.send(command, params[1])
        } else if (command === "USER") {
            console.Log("USER: ", params[1], params[2], params[3], params[4]);
            //connection.user(server, user, params[1], params[2], params[3], params[4]);
        } else if (command === "OPER") {
            //connection.oper(user, params[1], params[2]);
        } else if (command === "MODE" && params[1] !== undefined && params[1].charAt(0) !== '#') {
            //connection.mode(user, params[1], params[2]);
        } else if (command === "QUIT") {
            params.splice(0, 1)
            var messageQuit = params.join(" ");
            this.irc.disconnect(messageQuit, () => this.socket.emit('disconnectServer'));
        } else if (command === "SQUIT") {
            //connection.squit(server, user, params[1], params[2]);
        }
        // Channel operations
        else if (command === "JOIN") {
            console.log("Join Channel");
            this.irc.join(params[1]);
        } else if (command === "PART") {
            console.log("Part Channel", params[1], params[2]);
            this.irc.part(params[1], params[2], (channel, nick, reason, message) => this.socket.emit('part', channel, nick, reason, message));
        } else if (command === "MODE") {
            //channelOperations.mode(server, user, params[1], params[2]);
        } else if (command === "TOPIC") {
            //channelOperations.topic(server, user, params[1], params[2]);
        } else if (command === "NAMES") {
            //channelOperations.names(server, user, params[1]);
        } else if (command === "LIST") {
            //channelOperations.list(server, user, params[1]);
        } else if (command === "INVITE") {
            //channelOperations.invite(server, user, params[1], params[2]);
        } else if (command === "KICK") {
            //channelOperations.kick(server, user, params[1], params[2], params[3]);
        }
        // Sending messages
        else if (command === "PRIVMSG") {
            //messaging.privmsg(server, user, params[1], params[2]);
        } else if (command === "NOTICE") {
            //messaging.notice(server, user, params[1], params[2]);
        }
        // Server queries and commands
        else if (command === "MOTD") {
            // serverQuery.motd(server, user, params[1]);
        } else if (command === "LUSERS") {
            // serverQuery.lusers(server, user, params[1]);
        } else if (command === "VERSION") {
            //serverQuery.version(server, user, params[1]);
        } else if (command === "STATS") {
            //serverQuery.stats(server, user, params[1], params[2]);
        } else if (command === "LINKS") {
            //serverQuery.links(server, user, params[1]);
        } else if (command === "TIME") {
            //serverQuery.time(server, user, params[1]);
        } else if (command === "CONNECT") {
            //serverQuery.connect(server, user);
        } else if (command === "TRACE") {
            //serverQuery.trace(server, user, params[1]);
        } else if (command === "ADMIN") {
            //serverQuery.admin(server, user, params[1]);
        } else if (command === "INFO") {
            //serverQuery.info(server, user);
        }
        // User based queries
        else if (command === "WHO") {
            //userQuery.who(server, user, params[1]);
        } else if (command === "WHOIS") {
            this.irc.whois(params[1])
        } else if (command === "WHOWAS") {
            //userQuery.whowas(server, user, params[1]);
        }
        // Miscellaneous message
        else if (command === "KILL") {
            //miscellaneous.kill(server, user, params[1], params[2]);
        }
    } catch (err) {
        // Treat errors
        if (Array.isArray(err)) {
            user.error(err);
        } else {
            console.log(err);
        }
    }
}

module.exports = Client;