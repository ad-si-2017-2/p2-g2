webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/api.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_socket_io_client__ = __webpack_require__("../../../../socket.io-client/lib/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_socket_io_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ApiService = (function () {
    function ApiService(router) {
        this.router = router;
        this.registered = false;
        this.channels = [];
        this.channelsPage = 0;
        this.channelsOnPage = [];
        this.hasPreviousChannelsPage = false;
        this.hasNextChannelsPage = false;
        this.listingChannels = false;
        this.joiningChannel = false;
        this.userChannels = [];
        this.activeChannel = null;
        this.section = "server";
    }
    ApiService.prototype.register = function (nickname, server) {
        var _this = this;
        this.socket = __WEBPACK_IMPORTED_MODULE_1_socket_io_client__["connect"]("http://localhost:3000");
        this.socket.on('register', function (nick, motd) { return _this.onRegister(nick, motd); });
        this.socket.on('disconnectServer', function () { return _this.onDisconnectServer(); });
        this.socket.on('disconnect', function () { return _this.onDisconnect(); });
        this.socket.on('channellist', function (channels) { return _this.onList(channels); });
        this.socket.on('join', function (channel, nickname, message) { return _this.onJoin(channel, nickname, message); });
        this.socket.on('names', function (channel, nicks) { return _this.onNames(channel, nicks); });
        this.socket.on('part', function (channel, nick, reason, message) { return _this.onPart(channel, nick, reason, message); });
        this.socket.on('quit', function (nick, reason, channels, message) { return _this.onQuit(nick, reason, channels, message); });
        this.socket.on('kick', function (channel, nick, by, reason, message) { return _this.onKick(channel, nick, by, reason, message); });
        this.socket.on('kill', function (nick, reason, channels, message) { return _this.onKill(nick, reason, channels, message); });
        this.socket.on('message', function (nick, to, text, message) { return _this.onMessage(nick, to, text, message); });
        this.socket.on('whois', function (info) { return _this.onWhois(info); });
        this.socket.on('raw', function (message) { return _this.onRaw(message); });
        this.socket.on('nick', function (oldnick, newnick, channels, message) { return _this.onNick(oldnick, newnick, channels, message); });
        this.socket.emit('register', nickname, server);
        this.server = server;
    };
    ApiService.prototype.disconnect = function () {
        this.socket.emit('say', 'server', '/quit Saiu do Servidor!');
    };
    ApiService.prototype.list = function () {
        this.listingChannels = true;
        this.socket.emit('list');
    };
    ApiService.prototype.join = function (channel) {
        this.joiningChannel = true;
        this.socket.emit('join', channel);
    };
    ApiService.prototype.say = function (channel, message) {
        this.addMessageOnChannel(channel, this.nickname, message);
        this.socket.emit('say', channel, message);
    };
    ApiService.prototype.part = function (message) {
        var channelName = this.activeChannel['name'];
        this.socket.emit('part', channelName, message);
        console.log("part", channelName, message);
    };
    ApiService.prototype.send = function (command, args) {
        this.socket.emit('send', command, args);
    };
    ApiService.prototype.isRegistered = function () {
        return this.socket != null && this.socket.connected && this.registered;
    };
    ApiService.prototype.onRegister = function (nick, motd) {
        this.nickname = nick;
        this.motd = motd;
        this.registered = true;
        this.router.navigateByUrl('/chat');
    };
    ApiService.prototype.onDisconnectServer = function () {
        if (this.socket != null && this.socket.connected) {
            this.socket.disconnect();
        }
        this.socket = null;
        this.registered = false;
        this.router.navigateByUrl('/login');
    };
    ApiService.prototype.onDisconnect = function () {
        this.registered = false;
    };
    ApiService.prototype.onList = function (channels) {
        if (channels != null) {
            channels.sort(function (a, b) {
                if (a.users > b.users)
                    return -1;
                if (a.users < b.users)
                    return 1;
                if (a.name < b.name)
                    return -1;
                if (a.name > b.name)
                    return 1;
                return 0;
            });
            this.channels = channels;
        }
        this.listingChannels = false;
        this.channelsPage = 0;
        this.updateChannelsPage();
    };
    ApiService.prototype.onJoin = function (channel, nickname, message) {
        if (this.nickname != nickname) {
            this.addUserOnChannel(channel, nickname);
            this.addMessageOnChannel(channel, this.server, nickname + " entrou no canal.");
        }
    };
    ApiService.prototype.onNames = function (channel, nicks) {
        var channelObj = { 'name': channel, 'nicks': [], 'messages': [] };
        for (var _i = 0, _a = Object.keys(nicks); _i < _a.length; _i++) {
            var nick = _a[_i];
            channelObj.nicks.push(nick);
        }
        this.userChannels.push(channelObj);
        this.setSection(channel);
        this.addMessageOnChannel(channelObj.name, this.server, this.nickname + " entrou no canal.");
    };
    ApiService.prototype.onPart = function (channel, nick, reason, message) {
        if (channel != this.nickname) {
            if (this.nickname != nick) {
                this.removeUserOnChannel(channel, nick);
                this.addMessageOnChannel(channel, this.server, nick + " saiu do canal" + (reason != null ? (" (" + reason + ").") : "."));
            }
            else {
                this.removeUserChannel(channel);
            }
        }
    };
    ApiService.prototype.onQuit = function (nick, reason, channels, message) {
        for (var _i = 0, channels_1 = channels; _i < channels_1.length; _i++) {
            var channel = channels_1[_i];
            this.removeUserOnChannel(channel, nick);
            this.addMessageOnChannel(channel, this.server, nick + " saiu do servidor" + (reason != null ? (" (" + reason + ").") : "."));
        }
    };
    ApiService.prototype.onKick = function (channel, nick, by, reason, message) {
        console.log("kick", channel, nick, by, reason, message);
    };
    ApiService.prototype.onKill = function (nick, reason, channels, message) {
        console.log("kill", nick, reason, channels, message);
    };
    ApiService.prototype.onMessage = function (nick, to, text, message) {
        this.addMessageOnChannel(to, nick, text);
    };
    ApiService.prototype.onWhois = function (info) {
        if (this.activeChannel != null) {
            this.addMessageOnChannel(this.activeChannel['name'], this.server, info);
        }
    };
    ApiService.prototype.onRaw = function (message) {
        //console.log("raw", message);
    };
    ApiService.prototype.onNick = function (oldnick, newnick, channels, message) {
        if (this.nickname == oldnick) {
            this.nickname = newnick;
        }
        for (var _i = 0, channels_2 = channels; _i < channels_2.length; _i++) {
            var channel = channels_2[_i];
            this.removeUserOnChannel(channel, oldnick);
            this.addUserOnChannel(channel, newnick);
            this.addMessageOnChannel(channel, this.server, oldnick + " alterou o nick para " + newnick + ".");
        }
    };
    ApiService.prototype.updateChannelsPage = function () {
        this.channelsOnPage = this.channels.slice(this.channelsPage * 30, (this.channelsPage + 1) * 30);
        this.hasPreviousChannelsPage = this.channelsPage > 0;
        this.hasNextChannelsPage = this.channelsPage < (Math.ceil(this.channels.length / 30) - 1);
    };
    ApiService.prototype.nextChannelsPage = function () {
        if (this.hasNextChannelsPage) {
            this.channelsPage++;
            this.updateChannelsPage();
        }
    };
    ApiService.prototype.previousChannelsPage = function () {
        if (this.hasPreviousChannelsPage) {
            this.channelsPage--;
            this.updateChannelsPage();
        }
    };
    ApiService.prototype.setSection = function (section) {
        if (section != "server" && section != "channels") {
            this.activeChannel = this.getChannelByName(section);
        }
        this.section = section;
    };
    ApiService.prototype.getChannelByName = function (name) {
        for (var _i = 0, _a = this.userChannels; _i < _a.length; _i++) {
            var channel = _a[_i];
            if (channel['name'] == name) {
                return channel;
            }
        }
        return {};
    };
    ApiService.prototype.addMessageOnChannel = function (channelName, sender, message) {
        var date = new Date().toLocaleTimeString();
        var channel = this.getChannelByName(channelName);
        channel['messages'].push("<i>" + date + "</i>" + "&emsp;<b>" + sender + ":</b> " + message);
    };
    ApiService.prototype.addUserOnChannel = function (channelName, nick) {
        var channel = this.getChannelByName(channelName);
        channel['nicks'].push(nick);
    };
    ApiService.prototype.removeUserChannel = function (channelName) {
        this.section = "channels";
        var channel = this.getChannelByName(channelName);
        if (channel) {
            var index = this.userChannels.indexOf(channel);
            this.userChannels.splice(index, 1);
            this.activeChannel = null;
        }
    };
    ApiService.prototype.removeUserOnChannel = function (channelName, nick) {
        if (channelName != this.nickname) {
            var channel = this.getChannelByName(channelName);
            if (channel) {
                var index = channel['nicks'].indexOf(nick);
                if (index >= 0) {
                    channel['nicks'].splice(index, 1);
                }
            }
        }
    };
    ApiService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]])
    ], ApiService);
    return ApiService;
}());



/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent.prototype.ngOnInit = function () {
        /* this.socket = io.connect("http://localhost:3000");
        
        this.socket.on("registered", function(args) {
            console.log(args[0]);
            console.log(args[1]);
        }); */
    };
    AppComponent.prototype.onRegistered = function () {
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login_component__ = __webpack_require__("../../../../../src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__api_service__ = __webpack_require__("../../../../../src/app/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__dialog_dialog_component__ = __webpack_require__("../../../../../src/app/dialog/dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angular2_materialize__ = __webpack_require__("../../../../angular2-materialize/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__chat_chat_component__ = __webpack_require__("../../../../../src/app/chat/chat.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var appRoutes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: '#', redirectTo: '', pathMatch: 'full' },
    { path: 'login/#', redirectTo: '', pathMatch: 'full' },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_4__login_login_component__["a" /* LoginComponent */] },
    { path: 'chat', component: __WEBPACK_IMPORTED_MODULE_9__chat_chat_component__["a" /* ChatComponent */] },
    { path: '**', redirectTo: '/login', pathMatch: 'full' }
];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_4__login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_7__dialog_dialog_component__["a" /* DialogComponent */],
                __WEBPACK_IMPORTED_MODULE_9__chat_chat_component__["a" /* ChatComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_8_angular2_materialize__["a" /* MaterializeModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* RouterModule */].forRoot(appRoutes)
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_5__api_service__["a" /* ApiService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/chat/chat.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".server-side-item {\r\n    background-color: #1A237E !important;\r\n    white-space: nowrap;\r\n    overflow: hidden;         /* <- this does seem to be required */\r\n    text-overflow: ellipsis;\r\n}\r\n\r\n.channels-table {\r\n    overflow-x: auto;\r\n    white-space: nowrap;\r\n}\r\n\r\ntable {\r\n    table-layout: fixed;\r\n}\r\n  \r\ntd {\r\n    white-space: nowrap;\r\n    overflow: hidden;         /* <- this does seem to be required */\r\n    text-overflow: ellipsis;\r\n}\r\n\r\n.ellipsis {\r\n    white-space: nowrap;\r\n    overflow: hidden;         /* <- this does seem to be required */\r\n    text-overflow: ellipsis;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/chat/chat.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"out-container d-flex \">\r\n    <div class=\"sidebar\">\r\n        <div class=\"row\">\r\n            <div class=\"side-item\">\r\n                <a (click)=\"setSection('server')\">\r\n                    <div class=\"side-item-container\" [ngClass]=\"{'server-side-item': api.section == 'server'}\">\r\n                        <i class=\"fa fa-server\"></i>\r\n                        <span class=\"side-item-title\">Servidor</span>\r\n                    </div>\r\n                    <br>\r\n                </a>\r\n            </div>\r\n        </div>\r\n        <div class=\"row\">\r\n            <div class=\"side-item\">\r\n                <a (click)=\"setSection('channels')\">\r\n                    <div class=\"side-item-container\" [ngClass]=\"{'server-side-item': api.section == 'channels'}\">\r\n                        <i class=\"fa fa-cubes\"></i>\r\n                        <span class=\"side-item-title\">Canais</span>\r\n                    </div>\r\n                </a>\r\n            </div>\r\n        </div>\r\n        <div *ngFor=\"let channel of api.userChannels\" class=\"row\">\r\n            <div class=\"side-item\">\r\n                <a (click)=\"setSection(channel.name)\">\r\n                    <div class=\"side-item-container\" [ngClass]=\"{'server-side-item': api.section == channel.name}\" style=\"padding: 0 20px\">\r\n                        <i class=\"fa fa-cube\"></i>\r\n                        <span class=\"side-item-title ellipsis\">{{ channel.name }}</span>\r\n                    </div>\r\n                </a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div *ngIf=\"api.section == 'server'\" class=\"chat-container\">\r\n        <br>\r\n        <div class=\"row\">\r\n            <div class=\"col 4 offset-s1\">\r\n                <span class=\"title\">Servidor</span>\r\n            </div>\r\n            <br>\r\n        </div>\r\n        <div class=\"row\" style=\"padding-top: 10px;\">\r\n            <div class=\"col 4 offset-s1\">\r\n                <span class=\"header\">{{ api.motd }}</span>\r\n            </div>\r\n            <br>\r\n        </div>\r\n        <br>\r\n        <div class=\"row\">\r\n            <div class=\"col 4 offset-s1\">\r\n                <span class=\"subtitle\">INFORMAÇÕES</span>\r\n            </div>\r\n        </div>\r\n        <br>\r\n        <div class=\"row\">\r\n            <form class=\"col s10 offset-s1 card\" style=\"padding-top: 1rem; padding-bottom: 1rem\">\r\n                <div class=\"input-field col s3 align-self-center text-center\">\r\n                    <span>Nickname</span>\r\n                    <input disabled value=\"{{ api.nickname }}\" id=\"inickname\" type=\"text\" class=\"validate\">\r\n                    <!-- <label for=\"inickname\">Nickname</label> -->\r\n                </div>\r\n                <div class=\"input-field col s3\">\r\n                    <span>Servidor</span>\r\n                    <input disabled value=\"{{ api.server }}\" id=\"iserver\" type=\"text\" class=\"validate\">\r\n                    <!-- <label for=\"iserver\">Servidor</label> -->\r\n                </div>\r\n                <div class=\"input-field col s6 right-align\" (click)=\"disconnect()\">\r\n                    <a class=\"waves-light btn red\">SAIR</a>\r\n                </div>\r\n            </form>\r\n        </div>\r\n\r\n        <br>\r\n        <div class=\"row\">\r\n            <form class=\"col s10 offset-s1 card\" style=\"padding-top: 1rem; padding-bottom: 1rem\">\r\n                <div class=\"input-field col s4 left-align\" (click)=\"list()\">\r\n                    <a class=\"waves-light btn green\">LISTAR CANAIS</a>\r\n                </div>\r\n                <div *ngIf=\"!api.listingChannels\" class=\"col 4 offset-s1\">\r\n                    <span class=\"subtitle\">{{ api.listingChannels ? 'LISTANDO TODOS CANAIS...' : 'TODOS CANAIS (' + api.channels.length + ')' }}</span>\r\n                </div>\r\n            </form>\r\n        </div>\r\n        <br>\r\n        <div #divchannels *ngIf=\"!api.listingChannels\" class=\"row\">\r\n            <div class=\"col s10 offset-s1 channels-table card\">\r\n                <table class=\"highlight\">\r\n                    <thead>\r\n                        <td>\r\n                            Nome do canal\r\n                        </td>\r\n                        <td>\r\n                            Nº de usuários\r\n                        </td>\r\n                        <td>\r\n                            Tópico\r\n                        </td>\r\n                    </thead>\r\n                    <tr *ngFor=\"let channel of api.channelsOnPage\">\r\n                        <td>\r\n                            {{ channel.name }}\r\n                        </td>\r\n                        <td>\r\n                            {{ channel.users }}\r\n                        </td>\r\n                        <td>\r\n                            {{ channel.topic }}\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n            </div>\r\n        </div>\r\n        <br>\r\n        <div *ngIf=\"!api.listingChannels\" class=\"row\" style=\"padding-bottom: 40px !important;\">\r\n            <div class=\"col s10 offset-s1 center-align\">\r\n                <ul class=\"pagination\">\r\n                    <li [ngClass]=\"{'disabled': !api.hasPreviousChannelsPage}\" (click)=\"previousChannelsPage()\">\r\n                        <a>\r\n                            <i class=\"material-icons\">chevron_left</i>\r\n                        </a>\r\n                    </li>\r\n                    <li [ngClass]=\"{'disabled': !api.hasNextChannelsPage}\" (click)=\"nextChannelsPage()\">\r\n                        <a>\r\n                            <i class=\"material-icons\">chevron_right</i>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </div>\r\n        </div>\r\n        <div *ngIf=\"api.listingChannels\" class=\"row\">\r\n            <div class=\"input-field col s6 offset-s3 align-self-center text-center\" style=\"display: flex; justify-content: center;\">\r\n                <div class=\"preloader-wrapper active\">\r\n                    <div class=\"spinner-layer spinner-green-only\">\r\n                        <div class=\"circle-clipper left\">\r\n                            <div class=\"circle\"></div>\r\n                        </div>\r\n                        <div class=\"gap-patch\">\r\n                            <div class=\"circle\"></div>\r\n                        </div>\r\n                        <div class=\"circle-clipper right\">\r\n                            <div class=\"circle\"></div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div *ngIf=\"api.section == 'channels'\" class=\"chat-container\">\r\n        <br>\r\n        <div class=\"row\">\r\n            <div class=\"col 4 offset-s1\">\r\n                <span class=\"title\">Canais</span>\r\n            </div>\r\n            <br>\r\n        </div>\r\n        <br>\r\n        <div class=\"row\">\r\n            <div class=\"col 4 offset-s1\">\r\n                <span class=\"subtitle\">NOVO CANAL</span>\r\n            </div>\r\n        </div>\r\n        <br>\r\n        <div class=\"row\">\r\n            <div class=\"col s10 offset-s1 card\" style=\"padding-top: 1rem; padding-bottom: 1rem\">\r\n                <div class=\"input-field col s6 align-self-center text-center\">\r\n                    <span>Nome do canal</span>\r\n                    <input [(ngModel)]=\"newChannel\" type=\"text\" class=\"validate\">\r\n                    <!-- <label for=\"ichannel\">Nome do canal</label> -->\r\n                </div>\r\n                <div class=\"input-field col s6 right-align\" (click)=\"joinChannel()\">\r\n                    <a class=\"waves-light btn green\">ENTRAR</a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <br>\r\n        <div class=\"row\">\r\n            <div class=\"col 4 offset-s1\">\r\n                <span class=\"subtitle\">CANAIS ATIVOS</span>\r\n            </div>\r\n        </div>\r\n        <br>\r\n        <div *ngIf=\"api.userChannels.length > 0\" class=\"row\">\r\n            <div class=\"col s10 offset-s1 channels-table card\">\r\n                <table class=\"highlight\">\r\n                    <thead>\r\n                        <td>\r\n                            Nome do canal\r\n                        </td>\r\n                        <td>\r\n                            Nº de usuários\r\n                        </td>\r\n                    </thead>\r\n                    <tr *ngFor=\"let channel of api.userChannels\">\r\n                        <td>\r\n                            {{ channel.name }}\r\n                        </td>\r\n                        <td>\r\n                            {{ channel.nicks.length }}\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n            </div>\r\n        </div>\r\n        <div *ngIf=\"api.userChannels.length == 0\" class=\"row\" style=\"padding-top: 10px;\">\r\n            <div class=\"col 4 offset-s1\">\r\n                <span class=\"header\">Você ainda não entrou em nenhum canal.</span>\r\n            </div>\r\n            <br>\r\n        </div>\r\n        <br>\r\n    </div>\r\n    <div *ngIf=\"api.section != 'server' && api.section != 'channels'\">\r\n        <div class=\"chat-container\">\r\n            <br>\r\n            <div class=\"row\">\r\n                <div class=\"col s10 offset-s1\" style=\"padding: 0;\">\r\n                    <div class=\"col s8 align-self-center text-center\">\r\n                        <span class=\"title\">Canal {{ api.activeChannel.name }}</span>\r\n                    </div>\r\n                    <div class=\"col s4 right-align\" (click)=\"partChannel()\">\r\n                        <a class=\"waves-light btn red\">SAIR</a>\r\n                    </div>\r\n                </div>\r\n                <br>\r\n            </div>\r\n            <br>\r\n            <div class=\"row\">\r\n                <div class=\"col 4 offset-s1\">\r\n                    <span class=\"subtitle\">USUÁRIOS</span>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\" style=\"padding-top: 10px;\">\r\n                <div class=\"col 4 offset-s1\">\r\n                    <span class=\"header\">{{ api.activeChannel.nicks.join(', ') }}</span>\r\n                </div>\r\n                <br>\r\n            </div>\r\n            <br>\r\n            <div class=\"row\">\r\n                <div class=\"col 4 offset-s1\">\r\n                    <span class=\"subtitle\">MENSAGENS</span>\r\n                </div>\r\n            </div>\r\n            <br>\r\n            <div class=\"row\">\r\n                <div class=\"col s10 offset-s1 channels-table card\" style=\"padding: 20px; height: 300px; overflow: scroll;\" #scrollMe [scrollTop]=\"scrollMe.scrollHeight\">\r\n                    <h6 *ngFor=\"let message of api.activeChannel.messages\" style=\"padding: 10px 10px\" [innerHtml]=\"message\"></h6>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"col s10 offset-s1 card\" style=\"padding: 10px 20px\">\r\n                    <div class=\"input-field col s8 align-self-center text-center\">\r\n                        <input [(ngModel)]=\"message\" type=\"text\" placeholder=\"Digite sua mensagem...\" class=\"validate\">\r\n                    </div>\r\n                    <div class=\"input-field col s4 right-align\" (click)=\"sendMessage()\">\r\n                        <a class=\"waves-light btn indigo\">ENVIAR</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/chat/chat.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_service__ = __webpack_require__("../../../../../src/app/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ChatComponent = (function () {
    function ChatComponent(api, router) {
        this.router = router;
        this.message = "";
        this.newChannel = "";
        this.api = api;
        if (!api.isRegistered()) {
            this.router.navigateByUrl('/login');
        }
    }
    ChatComponent.prototype.ngOnInit = function () { };
    ChatComponent.prototype.list = function () {
        this.api.list();
    };
    ChatComponent.prototype.disconnect = function () {
        this.api.disconnect();
    };
    ChatComponent.prototype.nextChannelsPage = function (el) {
        this.api.nextChannelsPage();
    };
    ChatComponent.prototype.previousChannelsPage = function (el) {
        this.api.previousChannelsPage();
    };
    ChatComponent.prototype.setSection = function (section) {
        this.api.setSection(section);
    };
    ChatComponent.prototype.joinChannel = function () {
        if (!this.newChannel.startsWith("#")) {
            this.newChannel = "#" + this.newChannel;
        }
        this.api.join(this.newChannel);
        this.newChannel = "";
    };
    ChatComponent.prototype.sendMessage = function () {
        if (this.message.length > 0) {
            /*             if (this.message.startsWith("/")) {
                            var args = this.message.split(" ");
                            var command = args[0].substring(1);
                            args.splice(0, 1);
                            this.api.send(command, args.join(" "));
                        } else {
                            this.api.say(this.api.activeChannel['name'], this.message);
                        } */
            this.api.say(this.api.activeChannel['name'], this.message);
            this.message = "";
        }
    };
    ChatComponent.prototype.partChannel = function () {
        this.api.part("Saiu do Canal!");
    };
    ChatComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-chat',
            template: __webpack_require__("../../../../../src/app/chat/chat.component.html"),
            styles: [__webpack_require__("../../../../../src/app/chat/chat.component.css")],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["_10" /* ViewEncapsulation */].None
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]])
    ], ChatComponent);
    return ChatComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dialog/dialog.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DialogComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_materialize_css__ = __webpack_require__("../../../../materialize-css/dist/js/materialize.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_materialize_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_materialize_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var DialogComponent = (function () {
    function DialogComponent() {
        this.modalActions = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* EventEmitter */]();
        this.params = [];
        this.modelParams = [
            {
                dismissible: false,
                complete: function () { console.log('Closed'); }
            }
        ];
    }
    DialogComponent.prototype.openModal = function () {
        this.modalActions.emit({ action: "modal", params: ['open'] });
    };
    DialogComponent.prototype.closeModal = function () {
        this.modalActions.emit({ action: "modal", params: ['close'] });
    };
    DialogComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'dialog',
            template: "\n        <div id=\"modal\" class=\"modal bottom-sheet\" materialize=\"modal\" [materializeParams]=\"modelParams\" [materializeActions]=\"modalActions\">\n            <div class=\"modal-content\">\n                <h4>Modal Header 1</h4>\n                <p>A bunch of text</p>\n            </div>\n            <div class=\"modal-footer\">\n                <a class=\"waves-effect waves-green btn-flat\" (click)=\"closeModal()\">Close</a>\n                <a class=\"modal-action modal-close waves-effect waves-green btn-flat\">Agree</a>\n            </div>\n        </div>\n    "
        })
    ], DialogComponent);
    return DialogComponent;
}());



/***/ }),

/***/ "../../../../../src/app/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".wraper {\r\n    -ms-flex-item-align: center;\r\n        -ms-grid-row-align: center;\r\n        align-self: center;\r\n    width: 100%;\r\n}\r\n\r\n.preloader-wrapper {\r\n    margin: 0 auto;\r\n}\r\n\r\n.main-container {\r\n    padding: 0 !important;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"out-container d-flex \">\r\n    <div class=\"login-container\">\r\n        <div class=\"container\" style=\"height: 100%; display: flex;\r\n        justify-content: center;\">\r\n            <div class=\"wraper\" style=\"margin: 0 auto\">\r\n                <div class=\"row\">\r\n                    <h4 style=\"text-align: center; margin-bottom: 60px\">{{ connecting ? 'Conectando ao ' + server + '...' : 'Login IRC'}}</h4>\r\n                </div>\r\n                <div class=\"row\" style=\"padding-bottom: 20px;\">\r\n                    <div class=\"input-field col s6 offset-s3 align-self-center text-center\">\r\n                        <input [disabled]=\"connecting\" placeholder=\"Nickname\" [(ngModel)]=\"nickname\" type=\"text\" class=\"validate\" autocomplete=\"on\">\r\n                        <label for=\"nickname\">Nickname</label>\r\n                        <span *ngIf=\"!firstAttempt && nickname.length == 0\" style=\"color: red;\">Campo nickname é obrigatório.</span>\r\n                    </div>\r\n                </div>\r\n                <div class=\"row\" style=\"padding-bottom: 20px;\">\r\n                    <div class=\"input-field col s6 offset-s3\">\r\n                        <input [disabled]=\"connecting\" placeholder=\"Servidor\" [(ngModel)]=\"server\" type=\"text\" class=\"validate\" autocomplete=\"on\">\r\n                        <label for=\"server\">Servidor</label>\r\n                        <span *ngIf=\"!firstAttempt && server.length == 0\" style=\"color: red;\">Campo server é obrigatório.</span>\r\n                    </div>\r\n                </div>\r\n                <div *ngIf=\"connecting\" class=\"row\">\r\n                    <div class=\"input-field col s6 offset-s3 align-self-center text-center\" style=\"display: flex;\r\n                    justify-content: center;\">\r\n                        <div class=\"preloader-wrapper active\">\r\n                            <div class=\"spinner-layer spinner-green-only\">\r\n                                <div class=\"circle-clipper left\">\r\n                                    <div class=\"circle\"></div>\r\n                                </div>\r\n                                <div class=\"gap-patch\">\r\n                                    <div class=\"circle\"></div>\r\n                                </div>\r\n                                <div class=\"circle-clipper right\">\r\n                                    <div class=\"circle\"></div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div *ngIf=\"!connecting\" class=\"row\">\r\n                    <div class=\"input-field\" style=\"text-align: center; margin-top: 20px;\" (click)=\"login()\">\r\n                        <a id=\"btn-login\" class=\"waves-effect waves-light btn indigo\">ENTRAR</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__api_service__ = __webpack_require__("../../../../../src/app/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginComponent = (function () {
    function LoginComponent(api, router) {
        this.firstAttempt = true;
        this.connecting = false;
        this.nickname = "";
        this.server = "";
        this.router = router;
        this.api = api;
        if (api.isRegistered()) {
            this.router.navigateByUrl('/chat');
        }
    }
    LoginComponent.prototype.ngOnInit = function () {
        /*         this.nickname = "bbraz";
                this.server = "localhost";
                this.login(); */
    };
    LoginComponent.prototype.login = function () {
        if (this.nickname.length > 0 && this.server.length > 0) {
            this.connecting = true;
            this.api.register(this.nickname, this.server);
        }
        this.firstAttempt = false;
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'app-login',
            template: __webpack_require__("../../../../../src/app/login/login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/login/login.component.css")],
            encapsulation: __WEBPACK_IMPORTED_MODULE_1__angular_core__["_10" /* ViewEncapsulation */].None
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ }),

/***/ 1:
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map