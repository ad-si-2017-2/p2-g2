var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');
var Server = require('./domain/server');

// Create a new server
var server = new Server(io);
server.init();

// Send HTML index page
app.use(express.static('public'));
app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, '/index.html'));
});

http.listen(3000, function () {
    console.log('listening on port 3000');
});

