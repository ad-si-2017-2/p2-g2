import { DialogComponent } from './../dialog/dialog.component';
import { ApiService } from './../api.service';
import { Component, OnInit, ViewEncapsulation, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

declare var Materialize;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

    firstAttempt: boolean = true;
    connecting: boolean = false;
    nickname: string = "";
    server: string = "";
    api: ApiService;
    router: Router;

    constructor(api: ApiService, router: Router) {
        this.router = router;
        this.api = api;

        if (api.isRegistered()) {
            this.router.navigateByUrl('/chat');
        }
    }

    ngOnInit() {
/*         this.nickname = "bbraz";
        this.server = "localhost";
        this.login(); */
    }

    login() {
        if (this.nickname.length > 0 && this.server.length > 0) {
            this.connecting = true;
            this.api.register(this.nickname, this.server);
        }
        this.firstAttempt = false;
    }
}
