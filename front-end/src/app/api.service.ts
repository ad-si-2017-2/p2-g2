import { Injectable, ApplicationRef } from '@angular/core';
import * as io from 'socket.io-client';
import { Router } from '@angular/router';

@Injectable()
export class ApiService {

    socket: SocketIOClient.Socket;
    nickname: string;
    server: string;
    motd: string;
    registered: boolean = false;
    channels: Object[] = [];
    channelsPage: number = 0;
    channelsOnPage: Object[] = [];
    hasPreviousChannelsPage: boolean = false;
    hasNextChannelsPage: boolean = false;
    listingChannels: boolean = false;
    joiningChannel: boolean = false;
    userChannels: Object[] = [];
    activeChannel: Object = null;
    section: string = "server";

    constructor(private router: Router) {
    }

    register(nickname: string, server: string) {
        this.socket = io.connect("http://localhost:3000");
        this.socket.on('register', (nick, motd) => this.onRegister(nick, motd));
        this.socket.on('disconnectServer', () => this.onDisconnectServer());
        this.socket.on('disconnect', () => this.onDisconnect());
        this.socket.on('channellist', (channels) => this.onList(channels));
        this.socket.on('join', (channel, nickname, message) => this.onJoin(channel, nickname, message));
        this.socket.on('names', (channel, nicks) => this.onNames(channel, nicks));
        this.socket.on('part', (channel, nick, reason, message) => this.onPart(channel, nick, reason, message));
        this.socket.on('quit', (nick, reason, channels, message) => this.onQuit(nick, reason, channels, message));
        this.socket.on('kick', (channel, nick, by, reason, message) => this.onKick(channel, nick, by, reason, message));
        this.socket.on('kill', (nick, reason, channels, message) => this.onKill(nick, reason, channels, message));
        this.socket.on('message', (nick, to, text, message) => this.onMessage(nick, to, text, message));
        this.socket.on('whois', (info) => this.onWhois(info));
        this.socket.on('raw', (message) => this.onRaw(message));
        this.socket.on('nick', (oldnick, newnick, channels, message) => this.onNick(oldnick, newnick, channels, message));
        this.socket.emit('register', nickname, server);
        this.server = server;
    }

    disconnect() {
        this.socket.emit('say', 'server', '/quit Saiu do Servidor!');
    }

    list() {
        this.listingChannels = true;
        this.socket.emit('list');
    }

    join(channel: string) {
        this.joiningChannel = true;
        this.socket.emit('join', channel);
    }

    say(channel, message) {
        this.addMessageOnChannel(channel, this.nickname, message);
        this.socket.emit('say', channel, message);
    }

    part(message) {
        var channelName = this.activeChannel['name']; 
        this.socket.emit('part',channelName, message);      
        console.log("part", channelName, message);
    }

    send(command, args) {
        this.socket.emit('send', command, args);
    }

    isRegistered() {
        return this.socket != null && this.socket.connected && this.registered;
    }

    onRegister(nick, motd) {
        this.nickname = nick;
        this.motd = motd;
        this.registered = true;
        this.router.navigateByUrl('/chat');
    }

    onDisconnectServer(){
        if (this.socket != null && this.socket.connected) {
            this.socket.disconnect();
        }
        this.socket = null;
        this.registered = false;
        this.router.navigateByUrl('/login');
    }

    onDisconnect() {
        this.registered = false;   
    }

    onList(channels) {
        if (channels != null) {
            channels.sort(function (a, b) {
                if (a.users > b.users) return -1;
                if (a.users < b.users) return 1;
                if (a.name < b.name) return -1;
                if (a.name > b.name) return 1;
                return 0;
            })
            this.channels = channels;
        }
        this.listingChannels = false;
        this.channelsPage = 0;
        this.updateChannelsPage();
    }

    onJoin(channel, nickname, message) {
        if (this.nickname != nickname) {
            this.addUserOnChannel(channel, nickname);
            this.addMessageOnChannel(channel, this.server, nickname + " entrou no canal.");
        }
    }

    onNames(channel, nicks) {
        var channelObj = { 'name': channel, 'nicks': [], 'messages': [] };
        for (let nick of Object.keys(nicks)) {
            channelObj.nicks.push(nick);
        }
        this.userChannels.push(channelObj);
        this.setSection(channel);
        this.addMessageOnChannel(channelObj.name, this.server, this.nickname + " entrou no canal.");
    }

    onPart(channel, nick, reason, message) {
        if (channel != this.nickname){
            if (this.nickname != nick) {
                this.removeUserOnChannel(channel, nick);
                this.addMessageOnChannel(channel, this.server, nick + " saiu do canal" + (reason != null ? (" (" + reason + ").") : "."));
            } else {
                this.removeUserChannel(channel);
            }
        }
    }

    onQuit(nick, reason, channels, message) {
        for (let channel of channels) {
            this.removeUserOnChannel(channel, nick);
            this.addMessageOnChannel(channel, this.server, nick + " saiu do servidor" + (reason != null ? (" (" + reason + ").") : "."));
        }
    }

    onKick(channel, nick, by, reason, message) {
        console.log("kick", channel, nick, by, reason, message);
    }

    onKill(nick, reason, channels, message) {
        console.log("kill", nick, reason, channels, message);
    }

    onMessage(nick, to, text, message) {
        this.addMessageOnChannel(to, nick, text);
    }

    onWhois(info) {
        if (this.activeChannel != null) {
            this.addMessageOnChannel(this.activeChannel['name'], this.server, info);
        }
    }

    onRaw(message) {
        //console.log("raw", message);
    }

    onNick(oldnick, newnick, channels, message){

        if (this.nickname == oldnick) {
            this.nickname = newnick
        }

        for (let channel of channels) {
            this.removeUserOnChannel(channel, oldnick);
            this.addUserOnChannel(channel, newnick);
            this.addMessageOnChannel(channel, this.server, oldnick + " alterou o nick para " + newnick + ".");
        }
  
    }

    updateChannelsPage() {
        this.channelsOnPage = this.channels.slice(this.channelsPage * 30, (this.channelsPage + 1) * 30);
        this.hasPreviousChannelsPage = this.channelsPage > 0;
        this.hasNextChannelsPage = this.channelsPage < (Math.ceil(this.channels.length / 30) - 1);
    }

    nextChannelsPage() {
        if (this.hasNextChannelsPage) {
            this.channelsPage++;
            this.updateChannelsPage();
        }
    }

    previousChannelsPage() {
        if (this.hasPreviousChannelsPage) {
            this.channelsPage--;
            this.updateChannelsPage();
        }
    }

    setSection(section: string) {
        if (section != "server" && section != "channels") {
            this.activeChannel = this.getChannelByName(section);
        }
        this.section = section;
    }

    getChannelByName(name: string) {
        for (let channel of this.userChannels) {
            if (channel['name'] == name) {
                return channel;
            }
        }
        return {};
    }

    addMessageOnChannel(channelName: string, sender: string, message: string) {
        var date = new Date().toLocaleTimeString();
        var channel = this.getChannelByName(channelName);
        channel['messages'].push("<i>" + date + "</i>" + "&emsp;<b>" + sender + ":</b> " + message);
    }

    addUserOnChannel(channelName: string, nick: string) {
        var channel = this.getChannelByName(channelName);
        channel['nicks'].push(nick);
    }

    removeUserChannel(channelName){
        this.section = "channels";
        var channel = this.getChannelByName(channelName);
        if (channel){
            var index = this.userChannels.indexOf(channel);
            this.userChannels.splice(index, 1);
            this.activeChannel = null;
        }
    }

    removeUserOnChannel(channelName: string, nick: string) {
        if (channelName!=this.nickname){
            var channel = this.getChannelByName(channelName);
            if (channel) {
                var index = channel['nicks'].indexOf(nick);
                if (index >= 0) {
                    channel['nicks'].splice(index, 1);
                }
            }
        }
    }
}
