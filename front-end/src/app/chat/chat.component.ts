import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class ChatComponent implements OnInit {

    api: ApiService;
    message: string = "";
    newChannel: string = "";

    constructor(api: ApiService, private router: Router) {
        this.api = api;
        if (!api.isRegistered()) {
            this.router.navigateByUrl('/login');
        }
    }

    ngOnInit() {}

    list(){
        this.api.list();
    }

    disconnect() {
        this.api.disconnect();
    }

    nextChannelsPage(el) {
        this.api.nextChannelsPage();
    }

    previousChannelsPage(el) {
        this.api.previousChannelsPage();
    }

    setSection(section: string) {
        this.api.setSection(section);
    }

    joinChannel() {
        if (!this.newChannel.startsWith("#")) {
            this.newChannel = "#" + this.newChannel;
        }
        this.api.join(this.newChannel);
        this.newChannel = "";
    }

    sendMessage() {
        if (this.message.length > 0) {
/*             if (this.message.startsWith("/")) {
                var args = this.message.split(" ");
                var command = args[0].substring(1);
                args.splice(0, 1);
                this.api.send(command, args.join(" "));
            } else {
                this.api.say(this.api.activeChannel['name'], this.message);
            } */
            this.api.say(this.api.activeChannel['name'], this.message);
            this.message = "";
        }
    }

    partChannel() {
        this.api.part("Saiu do Canal!");
    }
}
